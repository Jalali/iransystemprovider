﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IranSystemProviderTester
{
    public partial class Form1 : Form
    {
        private readonly IranSystemProvider.IranSystemProvider _provider = null;
        public Form1()
        {
            InitializeComponent();
            _provider = new IranSystemProvider.IranSystemProvider();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox2.Text = _provider.GetIranSystemText(textBox1.Text);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
        }
    }
}
