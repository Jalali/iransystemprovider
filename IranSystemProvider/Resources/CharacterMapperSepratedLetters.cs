﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IranSystemProvider.Resources
{
    public class CharacterMapperSepratedLetters : IDictionaryMapper
    {
        private readonly IDictionary<byte, byte> _characterMapper = null;
        public CharacterMapperSepratedLetters()
        {
            _characterMapper = new Dictionary<byte, byte> {
                                    {48 , 128}, // 0
                                    {49 , 129}, // 1
                                    {50 , 130}, // 2
                                    {51 , 131}, // 3
                                    {52 , 132}, // 4
                                    {53 , 133}, // 5
                                    {54 , 134}, // 6
                                    {55 , 135}, // 7
                                    {56 , 136}, // 8
                                    {57 , 137}, // 9
                                    {161, 138}, // ،
                                    {191, 140}, // ؟
                                    {193, 143}, //ء 
                                    {194, 141}, // آ
                                    {195, 144}, // أ
                                    {196, 248}, //ؤ  
                                    {197, 144}, //إ
                                    {200, 146}, //ب 
                                    {201, 249}, //ة
                                    {202, 150}, //ت
                                    {203, 152}, //ث 
                                    {204, 154}, //ﺝ
                                    {205, 158}, //ﺡ
                                    {206, 160}, //ﺥ
                                    {207, 162}, //د
                                    {208, 163}, //ذ
                                    {209, 164}, //ر
                                    {210, 165},//ز
                                    {211, 167},//س
                                    {212, 169},//ش
                                    {213, 171}, //ص
                                    {214, 173}, //ض
                                    {216, 175}, //ط
                                    {217, 224}, //ظ
                                    {218, 225}, //ع
                                    {219, 229}, //غ
                                    {220, 139}, //-
                                    {221, 233},//ف
                                    {222, 235},//ق
                                    {223, 237},//ك
                                    {225, 241},//ل
                                    {227, 244},//م
                                    {228, 246},//ن
                                    {229, 249},//ه
                                    {230, 248},//و
                                    {236, 253},//ى
                                    {237, 253},//ي
                                    {129, 148},//پ
                                    {141, 156},//چ
                                    {142, 166},//ژ
                                    {152, 237},//ک
                                    {144, 239},//گ
            };
        }
        public IDictionary<byte, byte> CharacterMapper { get { return _characterMapper; } }
    }
}
